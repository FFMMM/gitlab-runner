package docker

import (
	"time"
	"github.com/docker/docker/client"
)

const DockerAPIVersion = client.DefaultVersion
const dockerLabelPrefix = "com.gitlab.gitlab-runner"

const prebuiltImageName = "gitlab/gitlab-runner-helper"
const prebuiltImageExtension = ".tar.xz"

const dockerCleanupTimeout = 5 * time.Minute
